#![allow(non_snake_case)]
#![allow(dead_code)]

use std::fs as fs;
use std::collections::{HashMap, HashSet};

fn main() {
    // AoC account id: 1278241-20201212-d0451b73

    // Day 1
    // day1_challenge1();
    // day1_challenge2();

    // Day 2
    // day2_challenge1();
    // day2_challenge2();

    // Day 3
    // day3_challenge1();
    // day3_challenge2();

    // Day 4
    // day4_challenge1();
    // day4_challenge2();

    // Day 5
    // day5_challenge1();
    // day5_challenge2();

    // Day 6
    // day6_challenge1();
    // day6_challenge2();

    // Day 7
    day7_challenge1();
    day7_challenge2();
}

// Day 5
fn day6_challenge1() {
    let input = read_text("/home/anieuwland/ontwikkeling/aoc2020/assets/day/6/input");

    let total_answers: usize = input
        .split("\n\n")
        .map(|group_answer| parse_answers(group_answer)
            .iter()
            .flat_map(|c| c)
            .collect::<HashSet<&char>>()
            .len()
        )
        .sum();

    println!("Total answers {:?}", total_answers);
}

fn day6_challenge2() {
    let input = read_text("/home/anieuwland/ontwikkeling/aoc2020/assets/day/6/input");

    let intersection_of_answers: usize = input
        .split("\n\n")
        .map(|group_answer| {
            let answers = parse_answers(group_answer);
            answers
                .iter()
                .skip(1)
                .fold(answers[0].clone(), |set1, set2| set1.intersection(set2).cloned().collect())
                .len()
        })
        .sum();

    println!("Intersection of answers: {:?}", intersection_of_answers);
}

fn parse_answers(group_answer: &str) -> Vec<HashSet<char>> {
    return group_answer.lines().map(|person_answer| person_answer.chars().collect()).collect();
}

// Day 5
fn day5_challenge1() {
    let input = read_text("/home/anieuwland/ontwikkeling/aoc2020/assets/day/5/input");
    let max_id = input.lines().map(|l| parse_boarding_pass(l).2).max();
    println!("{:?}", max_id);
}

fn day5_challenge2() {
    let rows = 0..128;
    let seats: Vec<usize> = (0..8).collect();
    let mut ids: Vec<usize> = vec![];

    for row in rows {
        for seat in seats.iter() {
            let id = row * 8 + seat;
            if id > 0 && id <= 822 { ids.push(id); }
            //ids.push(id);
        }
    }

    ids.sort();
    let input = read_text("/home/anieuwland/ontwikkeling/aoc2020/assets/day/5/input");
    for line in input.lines() {
        let (_, _, id) = parse_boarding_pass(line.trim());
        match ids.binary_search(&id) {
            Ok(id_pos) => { ids.remove(id_pos); },
            Err(_) => continue,
        }
    }

    println!("{:?} {:?}", ids.len(), ids);
}

fn parse_boarding_pass(def: &str) -> (usize, usize, usize) {
    let row_num = def[..7].chars().fold(0, |acc, c| acc << 1 | if c=='B' {1} else {0});
    let col_num = def[7..].chars().fold(0, |acc, c| acc << 1 | if c=='R' {1} else {0});
    return (row_num, col_num, row_num * 8 + col_num);
}

// Day 4
fn day4_challenge1() {
    let input = read_text("/home/anieuwland/ontwikkeling/aoc2020/assets/day/4/input");
    let passport_defs = input.split("\n\n");
    let passports: Vec<HashMap<&str, &str>> = passport_defs.map(|pd| parse_passport(pd)).collect();
    let valid_passports = passports.iter().filter(|p| valid_passport_1(p)).count();
    println!("{:?} valid passports", valid_passports);
}

fn day4_challenge2() {
    let input = read_text("/home/anieuwland/ontwikkeling/aoc2020/assets/day/4/input");
    let passport_defs = input.split("\n\n");
    let passports: Vec<HashMap<&str, &str>> = passport_defs.map(|pd| parse_passport(pd)).collect();
    let valid_passports = passports.iter().filter(|p| valid_passport_2(p)).count();
    println!("{:?} valid passports", valid_passports);
}

fn parse_passport(passport_src: &str) -> HashMap<&str, &str> {
    let keyvals: Vec<&str> = passport_src
        .lines()
        .flat_map(|l| l.split(' '))
        .collect();

    let mut passport = HashMap::new();
    for keyval in keyvals.iter() {
        let separated: Vec<&str> = keyval.split(':').collect();
        let key = separated[0];
        let val = separated[1];
        passport.insert(key, val);
    }

    return passport;
}

fn valid_passport_1(passport: &HashMap<&str, &str>) -> bool {
    let req_keys = vec!["byr", "iyr", "eyr", "hgt", "hcl", "ecl", "pid"];
    return req_keys.iter().all(|k| passport.contains_key(k));
}

fn valid_passport_2(passport: &HashMap<&str, &str>) -> bool {
    if !valid_passport_1(passport) { return false };

    let byr_str = passport["byr"];
    let byr_int = str::parse::<usize>(byr_str).unwrap();
    if byr_str.len() != 4 || byr_int < 1920 || byr_int > 2002 { return false };

    let iyr_str = passport["iyr"];
    let iyr_int = str::parse::<usize>(iyr_str).unwrap();
    if iyr_str.len() != 4 || iyr_int < 2010 || iyr_int > 2020 { return false };

    let eyr_str = passport["eyr"];
    let eyr_int = str::parse::<usize>(eyr_str).unwrap();
    if eyr_str.len() != 4 || eyr_int < 2020 || eyr_int > 2030 { return false };

    let hgt_str = passport["hgt"];
    let unit_pos = hgt_str.len() - 2;
    let unit = &hgt_str[unit_pos..];
    let invalidator = match unit {
        "cm" => |v: usize| v < 150 || v > 193,
        "in" => |v: usize| v < 59 || v > 76,
        _ => |_| true,
    };
    if unit != "cm" && unit != "in" { return false };
    let hgt_int = str::parse::<usize>(&hgt_str[..unit_pos]).unwrap();
    if invalidator(hgt_int) { return false };

    let hcl_chars: Vec<char> = passport["hcl"].chars().collect();
    let valid_chars = vec!['a', 'b', 'c', 'd', 'e', 'f', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9'];
    let valid_char = |c| valid_chars.contains(c);
    if hcl_chars[0] != '#' || !hcl_chars[1..].iter().all(valid_char) { return false };

    let ecl_str = passport["ecl"];
    let valid_colors = vec!["amb", "blu", "brn", "gry", "grn", "hzl", "oth"];
    if !valid_colors.iter().any(|color| color == &ecl_str) { return false };

    let pid_str = passport["pid"];
    let valid_chars = vec!['0', '1', '2', '3', '4', '5', '6', '7', '8', '9'];
    if pid_str.len() != 9 || !pid_str.chars().all(|c| valid_chars.contains(&c)) { return false };

    return true;
}

// Day 3
fn day3_challenge1() {
    let (slope_x, slope_y) = (3, 1);
    let trees_encountered = drive_for_slope(slope_x, slope_y);
    println!("{:?} trees encountered", trees_encountered);
}

fn day3_challenge2() {
    let slopes = vec![
        (1, 1),
        (3, 1),
        (5, 1),
        (7, 1),
        (1, 2),
    ];
    let te: Vec<usize> = slopes
        .iter()
        .map(|(slope_x, slope_y)| drive_for_slope(*slope_x, *slope_y))
        .collect();
    let o: usize = te.iter().product();
    println!("{:?} trees encountered, multiplied: {:?}", te, o);
}

fn drive_for_slope(slope_x: usize, slope_y: usize) -> usize {
    return read_text("/home/anieuwland/ontwikkeling/aoc2020/assets/day/3/input")
        .lines()
        .step_by(slope_y)
        .enumerate()
        .filter(|(step, trees)| trees.chars().cycle().nth(step * slope_x) == Some('#'))
        .count();
}

// Day 2
#[derive(Debug)]
struct Rule {
    min: usize,
    max: usize,
    subject: String,
}

fn day2_challenge1() {
    let input = read_text("/home/anieuwland/Ontwikkeling/aoc2020/assets/day/2/input");
    let mut passwords: Vec<(Rule, String)> = Vec::new();
    for line in input.lines() {
        let parts: Vec<&str> = line
            .split([':', '-', ' '].as_ref())
            .filter(|s| !s.is_empty())
            .collect();

        let min_val = str::parse::<usize>(parts[0]).unwrap();
        let max_val = str::parse::<usize>(parts[1]).unwrap();
        let subject = String::from(parts[2]);
        let password = parts[3];

        let final_rule = Rule { min: min_val, max: max_val, subject: subject };
        passwords.push((final_rule, String::from(password)));
    }

    fn is_valid(rule: &Rule, password: &String) -> bool {
        let count = password.matches(&rule.subject).count().into();
        return rule.min <= count && rule.max >= count;
    }

    let num_valid = passwords
        .iter()
        .filter(|(rule, password)| is_valid(rule, password))
        .count();
    let num_invalid = passwords.len() - num_valid;

    println!(
        "There were {:?} valid and {:?} invalid passwords",
        num_valid, num_invalid,
    );
}

fn day2_challenge2() {
    let input = read_text("/home/anieuwland/Ontwikkeling/aoc2020/assets/day/2/input");
    let mut passwords: Vec<(Rule, String)> = Vec::new();
    for line in input.lines() {
        let parts: Vec<&str> = line
            .split([':', '-', ' '].as_ref())
            .filter(|s| !s.is_empty())
            .collect();

        let min_val = str::parse::<usize>(parts[0]).unwrap();
        let max_val = str::parse::<usize>(parts[1]).unwrap();
        let subject = String::from(parts[2]);
        let password = parts[3];

        let final_rule = Rule { min: min_val, max: max_val, subject: subject };
        passwords.push((final_rule, String::from(password)));
    }

    fn is_valid(rule: &Rule, password: &String) -> bool {
        let mut chars = password.chars();
        let subject = rule.subject.chars().nth(0);
        let term1 = chars.nth(rule.min - 1) == subject;
        let term2 = chars.nth(rule.max - rule.min - 1) == subject;
        return term1 ^ term2;
    }

    let num_valid = passwords
        .iter()
        .filter(|(rule, password)| is_valid(rule, password))
        .count();
    let num_invalid = passwords.len() - num_valid;

    println!(
        "There were {:?} valid and {:?} invalid passwords",
        num_valid, num_invalid,
    );
}


// Day 1
fn day1_challenge1() {
    let input = read_text("/home/anieuwland/Ontwikkeling/AdventOfCode2020/assets/day/1/input");
    let mut nums: Vec<u32> = input
        .lines()
        .map(str::parse::<u32>)
        .filter(|v| v.is_ok())
        .map(|v| v.unwrap())
        .collect();
    nums.sort();

    let mut pairs: Vec<[u32; 3]> = Vec::with_capacity(2);
    for num in nums.iter() {
        let counterpart = 2020 - num;
        match nums.binary_search(&counterpart) {
            Err(_) => continue,
            Ok(_) => pairs.push([*num, counterpart, num * counterpart]),
        }
    }

    println!("{:?}", pairs);
}

fn day1_challenge2() {
    let input = read_text("/home/anieuwland/Ontwikkeling/AdventOfCode2020/assets/day/1/input");
    let mut nums: Vec<u32> = input
        .lines()
        .map(str::parse::<u32>)
        .filter(|v| v.is_ok())
        .map(|v| v.unwrap())
        .collect();
    nums.sort();

    let mut pairs: Vec<[u32; 4]> = Vec::with_capacity(2);
    for num1 in nums.iter() {
        for num2 in nums.iter() {
            if num1 + num2 > 2020 { continue };
            let counterpart = 2020 - num1 - num2;
            match nums.binary_search(&counterpart) {
                Err(_) => continue,
                Ok(_) => pairs.push([*num1, *num2, counterpart, num1 * num2 * counterpart]),
            }
        }
    }

    println!("{:?}", pairs);
}

// Helpers
fn read_text(filename: &str) -> String {
    let contents = fs::read_to_string(filename)
        .expect("Something went wrong reading the file");
    return contents;
}

